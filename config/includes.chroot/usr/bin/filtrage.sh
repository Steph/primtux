#!/bin/bash

gxmessage "Mise à jour du Contrôle parental
La mise à jour mensuelle est conseillée" -center -title "Mise à jour du contrôle parental" -font "Sans bold 10" -default "Cancel" -buttons "_Annuler":1,"_Mise à jour":2>/dev/null 

case $? in
	1)
		echo "Exit";;
	
	2)
		roxterm -e "sudo /usr/bin/CTMAJ.sh";;
esac
