#!/bin/bash

gxmessage " Installez webstrict après avoir mis à jour les dépôts." -center -title "Information" -font "Sans bold 10" -default "Cancel" -buttons "_Installer webstrict":1,"_Annuler":2>/dev/null 

case $? in
	1)
		roxterm -e sudo apt-get install webstrict-primtux;;

	2)
		echo "Exit";;
esac